. #!/bin/bash . 
echo "Welcome to my first job"

echo "Version of installed is"
sudo docker version

echo "Welcome to my first job step 2"
echo "list file and current working directory"
pwd ls -a

echo "Build docker image from dockerfile"

echo "List current images"
sudo docker images
echo "           "

sudo docker build --rm -t docker.io/jigar9981/jp2 .

echo "List current images"
sudo docker images
echo "           "

echo "List stop running container"
sudo docker stop newnginx1

echo "List container"
sudo docker rm newnginx1

echo "Run a container"
sudo docker run -d --name newnginx1 docker.io/jigar9981/jp2

sleep 5

echo "Test http connection within container newnginx1"

sudo docker exec newnginx1 sh -c "curl -I http://127.0.0.1"

sudo docker login -u $dockerUser -p $dockerPassword

sudo docker push docker.io/jigar9981/jp2

